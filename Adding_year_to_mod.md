## Description
This is a short guide on how to add years to your own mods.

Simply add a `<year>` entry in your mod's XML file under `<storeData>`. Your mod's XML should contain
something like

```
<vehicle>
    <storeData>
        <year>1999</year>
    </storeData>
</vehicle>
```

After this, the Vehicle Years mod should automatically read your mods provided year, instead of
reading the year provided by this mod.
