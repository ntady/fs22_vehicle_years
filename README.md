## Description
This mod adds vehicle years to all base-game vehicles (tractors, cars, trailers, seeders, etc.) in Farming Simulator 22.

This project also contains a few scripts to help collect and structure the data in a meaningful way.
